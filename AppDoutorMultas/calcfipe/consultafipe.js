var select_marcas;
var select_modelos;
var select_ano;
var tipo_veiculo;
var dados_veiculo;
var data_consulta;
var dv_preco;
var dv_name;
var dv_marca;
var dv_ano_modelo;
var dv_combustivel;
var dv_referencia;
var dv_fipe_codigo;
var slide_aberto;

jQuery(document).ready(function(){
	
	tipo_veiculo = "carros";
	select_marcas = document.getElementById("marcas");
	select_modelos = document.getElementById("modelos");
	select_ano = document.getElementById("ano");	
	dados_veiculo = document.getElementById("dados_veiculo");	
	data_consulta = document.getElementById("data_consulta");
	dv_preco = document.getElementById("dv_preco");
	dv_name = document.getElementById("dv_name");
	dv_marca = document.getElementById("dv_marca");
	dv_ano_modelo = document.getElementById("dv_ano_modelo");
	dv_combustivel = document.getElementById("dv_combustivel");
	dv_referencia = document.getElementById("dv_referencia");
	dv_fipe_codigo = document.getElementById("dv_fipe_codigo");

	slide_aberto = false;
	if (!slide_aberto){
		dados_veiculo.style.display = "none";
	}

	var hoje = new Date();
	var meses = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro'];
	data_consulta.innerHTML = "Data da consulta: " + hoje.getDate() + " de " + meses[hoje.getMonth()] + " de " + hoje.getFullYear();

	inicia_marcas();

	jQuery("#marcas").change(function(){		
		pesquisa_modelos(jQuery("#marcas").val());
		inicia_ano();		
	});

	jQuery("#modelos").change(function(){
		pesquisa_anos(jQuery("#marcas").val(),jQuery("#modelos").val());		
	});

	jQuery("#ano").change(function(){
		if (!slide_aberto){
			jQuery("#dados_veiculo").slideUp();
		}
	});

	jQuery("#tabelas button").click(function(){
		jQuery("#tabelas button").removeClass("tabela_ativa");
		jQuery(this).addClass("tabela_ativa");
	});


	jQuery("#processar").click(function(){

		if (jQuery("#marcas").val() == "cab" || jQuery("#modelos").val() == "cab" || jQuery("#ano").val()=="cab"){
			alert("Selecione a marca, modelo e ano do veículo!");
			return false;
		}

		pesquisa_valor(jQuery("#marcas").val(),jQuery("#modelos").val(),jQuery("#ano").val());		
	});

	jQuery("#btn_imprimir").click(function(){

		window.print();
		return;

		var conteudo = document.getElementById('dados_veiculo').innerHTML;
   		tela_impressao = window.open('Consulta de valores médios de veículos');

   		var cab = "<!doctype HTML>";
   		cab += "<html>";
   		cab += "<head>";
   		cab += "<title>Pesquisa tabela FIPE</title>";
   		cab += "<link href='https://fonts.googleapis.com/css?family=Noto+Serif' rel='stylesheet'>";
		cab += "<link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'/>";
		cab += "<link rel='stylesheet' href='style.css'>";		
		cab += "</head><body>";
   		//tela_impressao.document.write(cab);
		tela_impressao.document.write(conteudo);
		tela_impressao.document.write("</body></html>");
		tela_impressao.window.print();
		//tela_impressao.window.close();
	});

});

function inicia_marcas(){
	url = "https://fipeapi.appspot.com/api/1/" + tipo_veiculo + "/marcas.json";
	jQuery.ajax({
		url: url,
		dataType: 'jsonp',
		success: retornoMarcas
	});
	inicia_modelos();	
}

function tabela(ind){
	jQuery("#tabelas button").removeClass("tabela_ativa");
	jQuery(this).addClass("tabela_ativa");
	var veiculo = ['carros','motos','caminhoes'];
	tipo_veiculo = veiculo[ind];
	inicia_marcas();
}
function retornoMarcas(data){
	
	select_marcas.innerHTML = "";

	optMarca = document.createElement("option");
	optMarca.text = "Selecione a marca do veículo";
	optMarca.value = "cab";
	select_marcas.appendChild(optMarca);

	var opt;
	var array_marcas=[];

	try{
		for (x in data){
			text_marca = data[x].name + "##" + data[x].id;
			array_marcas.push(text_marca);
		}
	} catch(e){
		alert(e);
		alert(data);
	}

	array_marcas.sort();
	for (y in array_marcas){
		parts = array_marcas[y].split("##");
		opt = document.createElement("option");
		opt.text = parts[0];
		opt.value = parts[1];
		select_marcas.appendChild(opt);
	}
}

function inicia_modelos(){
	select_modelos.innerHTML = "";
	optModelos = document.createElement("option");
	optModelos.text = "Selecione o modelo do veículo";
	optModelos.value = "cab";
	select_modelos.appendChild(optModelos);
	inicia_ano();
}

function pesquisa_modelos(marca){

	url = "https://fipeapi.appspot.com/api/1/" + tipo_veiculo + "/veiculos/" + marca + ".json";
	jQuery.ajax({
		url: url,
		dataType: 'jsonp',
		success: retornoModelos
	});	
}


function retornoModelos(data){
	
	inicia_modelos();	

	var opt;

	try{
		for (x in data){
			opt = document.createElement("option");
			opt.text = data[x].name;
			opt.value = data[x].id;
			select_modelos.appendChild(opt);
		}
	} catch(e){
		alert(e);
		alert(data);
	}
}

function inicia_ano(){
	select_ano.innerHTML = "";
	optAno = document.createElement("option");
	optAno.text = "Selecione o ano do veículo";
	optAno.value = "cab";
	select_ano.appendChild(optAno);
	if (!slide_aberto){
		jQuery("#dados_veiculo").slideUp();
	}
}


function pesquisa_anos(marca,modelo){

	url = "https://fipeapi.appspot.com/api/1/" + tipo_veiculo + "/veiculo/" + marca + "/" + modelo + ".json";
	jQuery.ajax({
		url: url,
		dataType: 'jsonp',
		success: retornoAnos
	});	
}


function retornoAnos(data){
	
	inicia_ano();	

	var opt;

	try{
		for (x in data){
			opt = document.createElement("option");
			opt.text = data[x].name;
			opt.value = data[x].id;
			select_ano.appendChild(opt);
		}
	} catch(e){
		alert(e);
		alert(data);
	}
}



function pesquisa_valor(marca,modelo,ano){

	url = "https://fipeapi.appspot.com/api/1/" + tipo_veiculo + "/veiculo/" + marca + "/" + modelo + "/" + ano + ".json";
	jQuery.ajax({
		url: url,
		dataType: 'jsonp',
		success: retornoValor
	});	
}


function retornoValor(data){	

	dv_preco.innerHTML = data.preco;
	dv_name.innerHTML = data.name;
	dv_marca.innerHTML = data.marca;
	dv_ano_modelo.innerHTML = data.ano_modelo;
	dv_combustivel.innerHTML = data.combustivel;
	dv_referencia.innerHTML = data.referencia;
	dv_fipe_codigo.innerHTML = data.fipe_codigo;

	jQuery("#dados_veiculo").slideDown();	
	return;
}