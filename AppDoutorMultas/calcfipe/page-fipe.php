<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="consultafipe.js"></script>
<div class="augesystems">
	<div id="faixa-divisao" class="container-fluid screen-only">
	</div>	
	<div class="container">
		
		<div id="tabelas" class="screen-only">
			<div class="col-md-8 col-md-offset-2 grupo_btn">	
				<div>
					<button class="tabela_ativa" onclick="tabela(0);">Carros</button><button onclick="tabela(1);">Motos</button><button onclick="tabela(2);">Caminhões</button>								
				</div>
			</div>
		</div>

		<div id="selveiculo" class="row screen-only">
			<div class="col-md-8 col-md-offset-2">
				<h2>Informe primeiramente a marca do veículo que deseja consultar, e em seguida informe o modelo e o ano do mesmo:</h2>
				<select id="marcas" class="form-control">
					<option>Marca</option>
					<option>Ford</option>
					<option>Chevrolet</option>
					<option>Volksvagem</option>
				</select>
				<select id="modelos" class="form-control">
					<option>Modelo</option>
				</select>
				<select id="ano"  class="form-control">
					<option>Ano</option>
				</select>
				<button id="processar">Consultar</button>				
			</div> 
		</div>

		<div id="dados_veiculo" class="row">
			<div class="col-md-8 col-md-offset-2">
				<h2>Dados do veículo</h2>
				<div class="dados_linha row">
					<div class="dados_titulo col-md-5 col-sm-5 col-xs-12">
						<span>Nome:</span>
					</div>
					<div class="dados_valor col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 col-xs-offset-0">
						<span id="dv_name">Não sei</span>
					</div>
				</div>
				<div class="dados_linha row">
					<div class="dados_titulo col-md-5 col-sm-5 col-xs-12">
						<span>Código Fipe:</span>
					</div>
					<div class="dados_valor col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 col-xs-offset-0">
						<span id="dv_fipe_codigo">Não sei</span>
					</div>
				</div>
				<div class="dados_linha row">
					<div class="dados_titulo col-md-5 col-sm-5 col-xs-12">
						<span>Marca:</span>
					</div>
					<div class="dados_valor col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 col-xs-offset-0">
						<span id="dv_marca">Não sei</span>
					</div>
				</div>
				<div class="dados_linha row">
					<div class="dados_titulo col-md-5 col-sm-5 col-xs-12">
						<span>Ano/Modelo:</span>
					</div>
					<div class="dados_valor col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 col-xs-offset-0">
						<span id="dv_ano_modelo">Não sei</span>
					</div>
				</div>
				<div class="dados_linha row">
					<div class="dados_titulo col-md-5 col-sm-5 col-xs-12">
						<span>Combustível:</span>
					</div>
					<div class="dados_valor col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 col-xs-offset-0">
						<span id="dv_combustivel">Não sei</span>
					</div>
				</div>
				<div class="dados_linha row">
					<div class="dados_titulo col-md-5 col-sm-5 col-xs-12">
						<span>Referência:</span>
					</div>
					<div class="dados_valor col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 col-xs-offset-0">
						<span id="dv_referencia">Não sei</span>
					</div>
				</div>
				<div class="dados_linha row">
					<div class="dados_titulo col-md-5 col-sm-5 col-xs-12">
						<span id="dt_preco">Valor médio:</span>
					</div>
					<div class="dados_valor col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2 col-xs-12 col-xs-offset-0">
						<span id="dv_preco">Não sei</span>
					</div>
				</div>
				<span id="data_consulta">data da consulta: 10 de janeiro de 2018</span>
				<div class="row">
					<button id="btn_imprimir">Imprimir</button>
				</div>
			</div> 
		</div>

	</div>

</div>




