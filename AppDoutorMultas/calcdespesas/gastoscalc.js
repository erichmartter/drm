var slide_aberto;

var sem_desval_dia;
var sem_desval_mes;
var sem_desval_ano;
var com_desval_dia;
var com_desval_mes;
var com_desval_ano;

var dados_veiculo;

var carro_valor;
var carro_ano;
var lavagem;
var flanelinha;
var estacionamento;
var combustivel;
var financiamento;
var impostos;
var multas;
var manutencao;
var revisao;
var seguro;
var franquia;
var outros;

jQuery(document).ready(function(){	
    
    	sem_desval_dia = document.getElementById("sem_desval_dia");
	sem_desval_mes = document.getElementById("sem_desval_mes");	
	sem_desval_ano = document.getElementById("sem_desval_ano");
	com_desval_dia = document.getElementById("com_desval_dia");	
	com_desval_mes = document.getElementById("com_desval_mes");
	com_desval_ano = document.getElementById("com_desval_ano");
        
        dados_veiculo = document.getElementById("dados_veiculo");
        
	carro_valor = document.getElementById("i_carro_valor");
	carro_ano = document.getElementById("i_carro_ano");
	lavagem = document.getElementById("i_lavagem");
	flanelinha = document.getElementById("i_flanelinha");
	estacionamento = document.getElementById("i_estacionamento");
	combustivel = document.getElementById("i_combustivel");
	financiamento = document.getElementById("i_financiamento");
	impostos = document.getElementById("i_impostos");
	multas = document.getElementById("i_multas");
	manutencao = document.getElementById("i_manutencao");
	revisao = document.getElementById("i_revisao");
	seguro = document.getElementById("i_seguro");
	franquia = document.getElementById("i_franquia");
	outros = document.getElementById("i_outros");

	slide_aberto = false;

	if (!slide_aberto){
		dados_veiculo.style.display = "none";
	}

	
	jQuery("#processar").click(function(){
		calcular_valores();
                return;		
	});
        
       
	jQuery("#btn_imprimir").click(function(){
		window.print();
		return;		
	});

	jQuery(".augesystems input").keypress(entrada_numeros);

	var input = document.querySelector('input');	
});

function formata_valor(num){

	valor =	Number(num).toLocaleString("pt-BR", {style: 'currency',currency: 'BRL'});

	return valor;
}

function entrada_numeros(e){

	var char = e.keyCode || e.which;    
	var letra = String.fromCharCode(char).toLowerCase();
	e.preventDefault();
	novo_value = this.value + letra;

	var apenas_numeros = novo_value.replace(/[^0-9]/g,"");

	var calculado = apenas_numeros;

	if (calculado.length > 2){
		calculado = formata_valor(Number(apenas_numeros)/100);
	}

	this.value = calculado.replace('R$','');

	return;
}

function currency_to_float(num){

	valor = num.replace(".","").replace(',','.');
	val_float = parseFloat(valor);
	return val_float;
}

function calcular_valores(){	
    

	inputs = document.getElementsByTagName("input");
	for (y in inputs){
		if (inputs[y].value == ""){
			inputs[y].value = "0,00";
		}
	}
        
	
	var gasto_anual = 
	(currency_to_float(lavagem.value) * 12) + 
	(currency_to_float(flanelinha.value) * 12) + 
	(currency_to_float(estacionamento.value) * 12) + 
	(currency_to_float(combustivel.value) * 12) + 
	(currency_to_float(financiamento.value) * 12) + 
	currency_to_float(impostos.value) +
	currency_to_float(multas.value) +
	currency_to_float(manutencao.value) +
	currency_to_float(revisao.value) +
	currency_to_float(seguro.value) + 
	currency_to_float(franquia.value) +
	currency_to_float(outros.value);
        

        sem_desval_ano.innerHTML = formata_valor(gasto_anual);
	sem_desval_mes.innerHTML = formata_valor(gasto_anual / 12);
	sem_desval_dia.innerHTML = formata_valor(gasto_anual / 365);
        
        
	valor_numerico = currency_to_float(carro_valor.value);
	valor_alterado2 = (valor_numerico *0.11);
	gasto_anual_desval = gasto_anual + valor_alterado2;
        

	com_desval_ano.innerHTML = formata_valor(gasto_anual_desval);
	com_desval_mes.innerHTML = formata_valor(gasto_anual_desval / 12);
	com_desval_dia.innerHTML = formata_valor(gasto_anual_desval / 365);

       
	jQuery("#dados_veiculo").slideDown();	
	return;

}

