var slide_aberto;

var resultado;

var dados_veiculo;


var km;
var litros;

jQuery(document).ready(function(){	
    
    	resultado = document.getElementById("sem_desval_dia");
	
        dados_veiculo = document.getElementById("dados_veiculo");
        
	km = document.getElementById("km");
	litros = document.getElementById("litros");

	slide_aberto = false;

	if (!slide_aberto){
		dados_veiculo.style.display = "none";
	}

	
	jQuery("#processar").click(function(){
		calcular_valores();
                return;		
	});
        
       
	jQuery("#btn_imprimir").click(function(){
		window.print();
		return;		
	});

	jQuery(".augesystems input").keypress(entrada_numeros);

	var input = document.querySelector('input');	
});

function formata_valor(num){

	valor =	Number(num).toLocaleString("pt-BR", {style: 'currency',currency: 'BRL'});

	return valor;
}

function entrada_numeros(e){

	var char = e.keyCode || e.which;    
	var letra = String.fromCharCode(char).toLowerCase();
	e.preventDefault();
	novo_value = this.value + letra;

	var apenas_numeros = novo_value.replace(/[^0-9]/g,"");

	var calculado = apenas_numeros;

	if (calculado.length > 2){
		calculado = formata_valor(Number(apenas_numeros)/100);
	}

	this.value = calculado.replace('R$','');

	return;
}

function currency_to_float(num){

	valor = num.replace(".","").replace(',','.');
	val_float = parseFloat(valor);
	return val_float;
}

function calcular_valores(){	
    

	inputs = document.getElementsByTagName("input");
	for (y in inputs){
		if (inputs[y].value == ""){
			inputs[y].value = "0,00";
		}
	}
        
	
	var gasto_anual = 
	currency_to_float(km.value) /
	currency_to_float(litros.value);
        


	valor_numerico = currency_to_float(carro_valor.value);
	valor_alterado2 = (valor_numerico *0.11);
	gasto_anual_desval = gasto_anual + valor_alterado2;
       
       
	jQuery("#dados_veiculo").slideDown();	
	return;

}

