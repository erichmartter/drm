<html lang="pt-BR" prefix="og: http://ogp.me/ns#" class="no-js"><head>
<meta charset="UTF-8">
<title>Calculadora de despesas do carro · Doutor Multas</title>
<script type="text/javascript" async="" data-rocketsrc="https://cdn.pushcrew.com/js/5429300d063efac8d3dacbc42b804968.js" data-rocketoptimized="true"></script><script async="" src="//www.googletagmanager.com/gtm.js?id=GTM-N834XP"></script><script type="text/javascript">
//<![CDATA[
window.__cfRocketOptions = {byc:0,p:1518724108,petok:"c016d4f6a97b9c51644d63fde5a164b956afb182-1519387049-1800"};
//]]>
</script>
<script type="text/javascript" src="https://ajax.cloudflare.com/cdn-cgi/scripts/b7ef205d/cloudflare-static/rocket.min.js"></script>
<script type="text/rocketscript" data-rocketoptimized="true">
					var bhittani_plugin_kksr_js = {"nonce":"721cf4238f","grs":true,"ajaxurl":"https:\/\/doutormultas.com.br\/wp-admin\/admin-ajax.php","func":"kksr_ajax","msg":"Esse texto te ajudou?","fuelspeed":400,"thankyou":"Obrigado por nos avaliar!","error_msg":"Ocorreu um erro","tooltip":"1","tooltips":[{"tip":"","color":"#ffffff"},{"tip":"","color":"#ffffff"},{"tip":"","color":"#ffffff"},{"tip":"","color":"#ffffff"},{"tip":"","color":"#ffffff"}]};
				</script>

<script type="text/rocketscript" data-rocketoptimized="true">
(function(p,u,s,h) {
    p._pcq = p._pcq || [];
    p._pcq.push(['_currentTime', Date.now()]);
    s = u.createElement('script'); s.type = 'text/javascript'; s.async = true;
    s.src = 'https://cdn.pushcrew.com/js/5429300d063efac8d3dacbc42b804968.js';
    h = u.getElementsByTagName('script')[0]; h.parentNode.insertBefore(s, h);
})(window,document);
</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="generator" content="Épico 1.9.4">

<link rel="canonical" href="https://doutormultas.com.br/calcdespesas/">
<meta property="og:locale" content="pt_BR">
<meta property="og:type" content="article">
<meta property="og:title" content="Calculadora de despesas do carro · Doutor Multas">
<meta property="og:url" content="https://doutormultas.com.br/calcdespesas/">
<meta property="og:site_name" content="Doutor Multas">
<meta property="article:publisher" content="https://www.facebook.com/doutormultas">
<meta property="article:author" content="https://www.facebook.com/doutormultas">

<link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com">
<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//s.w.org">
<link rel="dns-prefetch" href="//themes.googleusercontent.com">
<link rel="alternate" type="application/rss+xml" title="Feed para Doutor Multas »" href="https://doutormultas.com.br/feed/">
<link rel="alternate" type="application/rss+xml" title="Feed de comentários para Doutor Multas »" href="https://doutormultas.com.br/comments/feed/">
<script type="text/rocketscript" data-rocketoptimized="true">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/doutormultas.com.br\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.4"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<link rel="stylesheet" id="epico_global_assets-epico_capture_styles-css" href="https://doutormultas.com.br/wp-content/plugins/uf-epico/assets/css/capture-styles-min.css?ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="gallery-css" href="https://doutormultas.com.br/wp-content/themes/epico/core/css/gallery.min.css?ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="epico-icons-css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.7.0" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-css" href="https://doutormultas.com.br/wp-content/themes/epico-jr/bootstrap/css/bootstrap.min.css?ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="parent-css" href="https://doutormultas.com.br/wp-content/themes/epico/style.min.css?ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="style-css" href="https://doutormultas.com.br/wp-content/themes/epico-jr/style.css?ver=4.9.4" type="text/css" media="all">
<style id="style-inline-css" type="text/css">
.breadcrumb-trail .trail-item:nth-child(n+4) span:before,.breadcrumb-trail .trail-end,.epico-related-posts > h3.epico-related-posts-title,#respond,.epico-related-posts .fa-plus-square-o:before,.comment-respond label[for="author"]:before,.comment-respond label[for="email"]:before,.comment-respond label[for="url"]:before,.comment-respond label[for="comment"]:before,ia-info-toggle:after{color:rgba(0,0,0,.2)}.epico-related-posts>h3.epico-related-posts-title{border-bottom: 1px solid rgba(0,0,0,.2)}.logged-in-as{border: 1px solid rgba(0,0,0,.2)}.breadcrumb-trail .trail-item a,[class*="epc-s"] .epico-related-posts a,[class*="epc-s"] #respond a,[class*="epc-s"] #respond a:hover,[class*="epc-s"] #respond .logged-in-as:before,[class*="epc-s"] #breadcrumbs a{color: rgba(0,0,0,.5)}.zen .breadcrumb-trail .trail-item:nth-child(n+4) span:before,.zen .breadcrumb-trail .trail-end,.zen .epico-related-posts > h3.epico-related-posts-title,.zen #respond,.zen .epico-related-posts .fa-plus-square-o:before,.zen .comment-respond label[for="author"]:before,.zen .comment-respond label[for="email"]:before,.zen .comment-respond label[for="url"]:before,.zen .comment-respond label[for="comment"]:before,.zen ia-info-toggle:after,.zen #comments-template label{color:#777}.zen .epico-related-posts>h3.epico-related-posts-title{border-bottom: 1px solid #e4e4e4}.zen .logged-in-as{border: 1px solid #A1A1A1}.zen .breadcrumb-trail .trail-item a,.zen[class*="epc-s"] .epico-related-posts a,.zen[class*="epc-s"] #respond a,.zen[class*="epc-s"] #respond a:hover,.zen[class*="epc-s"] #respond .logged-in-as:before{color:#777}
.page-template-landing[class*="epc-"]{background:#ffffff;}.page-template-landing[class*="epc-"] #page,.page-template-tpl-helper-min-pb[class*="epc-"] #page{border-top: none !important}
.epc-nst,.epc-nst label,.epc-nst textarea,.epc-nst input:not([type=submit]):not([type=radio]):not([type=checkbox]):not([type=file]),.epc-nst select[multiple=multiple],.epc-nst.epc-button,.epc-nst input[type="submit"],.epc-nst a.uf-button,button.uf-button,.epc-nst .not-found input.search-submit[type="submit"],.epc-nst #nav input.search-submit[type="submit"],.epc-nst #comments .comment-reply-link,.epc-nst #comments .comment-reply-login,.epc-nst .widget_epico_author-id a[class*="button"],.epc-nst.wordpress div.uberaviso a[class*="button"],.epc-nst.wordpress .mejs-controls a:focus>.mejs-offscreen,.epc-nst .format-quote p:first-child:before,.epc-nst .format-quote p:first-child:after,.epc-nst .epico-related-posts h4.related-post-title,.epc-nst .placeholder,.epc-nst .editor-tag{font-family:Noto Serif,Georgia,serif}.epc-nst li.fa,.epc-nst li.fa:before,.epc-nst .fa,.epc-nst textarea.fa,.epc-nst input:not([type=submit]):not([type=radio]):not([type=checkbox]):not([type=file]).fa,.epc-nst .not-found input.search-submit[type="submit"],.epc-nst #search-wrap input.fa[type="search"],.epc-nst #nav input.search-submit[type="submit"],.epc-nst #respond #submit,.epc-nst .capture-wrap form input[class*="uf-"]{font-family:FontAwesome,Noto Serif,Georgia,serif!important}.epc-nst main{font-size:.9rem}.epc-nst #menu-primary li a,.epc-nst .author-profile,.epc-nst .nav-posts span,.epc-nst #sidebar-promo-inner .widget,.epc-nst #branding,.epc-nst #sidebar-footer .widget{font-size:.78889rem}.epc-nst .entry-byline>*,.epc-nst .entry-footer>*{font-size:.69012rem}.epc-nst #sidebar-primary section[class*="epico_pages"] li>a:first-child,.epc-nst #sidebar-primary section[class*="epico_links"] li>a:first-child{font-size:1.06563rem}.epc-nst .widget h3,.epc-nst .widget_social-id h3{font-size:1.30181rem}.epc-nst textarea,.epc-nst input:not([type=submit]):not([type=radio]):not([type=checkbox]):not([type=file]){font-size:.9rem}.epc-nst .nav-posts{font-size:1.025rem}.epc-nst #sidebar-top .widget,.epc-nst #breadcrumbs nav,.epc-nst .credit{font-size:.69012rem}.epc-nst .uberaviso{font-size:16px}@media only screen and (min-width:480px){.epc-nst .epico-related-posts h4.related-post-title{font-size:.9rem}}@media only screen and (min-width:680px){.epc-nst #sidebar-primary .widget,.epc-nst #after-primary,.epc-nst #sidebar-promo-home .widget,.epc-nst #sidebar-subsidiary .widget,.epc-nst #sidebar-before-content .widget,.epc-nst #after-primary .widget{font-size:.78889rem}.epc-nst #search-toggle:after{top:1px}}@media only screen and (min-width:1020px){.epc-nst.gecko #search-toggle::before,.epc-nst.ie #search-toggle::before{top:30px}.epc-nst .capture-wrap.fw .capture .capture-intro{font-size:19px}}@media only screen and (min-width:1410px){.epc-nst.gecko #search-toggle::before,.epc-nst.ie #search-toggle::before{top:33px}.epc-nst .capture-wrap.fw .capture .capture-notice{font-size:13px}.epc-nst .capture-wrap.fw.ip .capture .uf-fields .capture-notice{font-size:24px}.epc-nst #search-toggle:after{top:0}}@media only screen and (max-width:680px){.epc-nst #search-toggle:after{right:19px}.epc-nst #menu-primary li a{font-size:1.125rem}}@media only screen and (max-width:480px){.epc-nst #search-toggle:after{right:20px}}
#sidebar-primary .widget_epico_pop-id{background-color:#ffffff !important;color:#498ee3 !important;border-bottom: 10px solid #e8e8e8 !important}ul.uf_epicoepico_pop-list a.uf_epicoepico_pop-link{color:#498ee3 !important}h3.uf_epicoepico_pop-title{background:#e8e8e8 !important;color:#333333 !important}h3.uf_epicoepico_pop-title:before{color:#8c8c8c !important}ul.uf_epicoepico_pop-list li:before{color:rgba(0, 0, 0, 0.2) !important}ul.uf_epicoepico_pop-list li:hover:before{color:rgba(0, 0, 0, 0.4) !important}
#cw-uf_epicoepico_capture_widget{border:0px;background-color:#0075bb;}#cw-uf_epicoepico_capture_widget.fw.capture-wrap .capture-form{border-bottom:10px solid #c6c6c6 !important}.epc-s4.plural #cw-uf_epicoepico_capture_widget.fw,.epc-s5.plural #cw-uf_epicoepico_capture_widget.fw,.epc-s6.plural #cw-uf_epicoepico_capture_widget.fw{border-bottom: none}#cw-uf_epicoepico_capture_widget .capture:nth-child(2){background-color:#eff1f1 !important}#cw-uf_epicoepico_capture_widget .capture .capture-title{color:#ffffff !important}#cw-uf_epicoepico_capture_widget .capture .capture-icon i:before,#cw-uf_epicoepico_capture_widget .capture-inner .capture-iconinner i:before{color:#B3C1C7 !important;text-shadow:none !important}#cw-uf_epicoepico_capture_widget .capture .capture-intro{color:#ffffff !important}#cw-uf_epicoepico_capture_widget .capture .capture-intro,#cw-uf_epicoepico_capture_widget .capture .capture-intro *{color:#ffffff}#cw-uf_epicoepico_capture_widget .capture .capture-notice,#cw-uf_epicoepico_capture_widget .capture .capture-notice *{color:#687E87}#cw-uf_epicoepico_capture_widget.ip .capture .capture-notice,#cw-uf_epicoepico_capture_widget.ip .capture .capture-notice *{color:#ffffff}#cw-uf_epicoepico_capture_widget .capture form .uf-email,#cw-uf_epicoepico_capture_widget .capture form .uf-name{background-color:#FFFFFF;border:1px solid rgba(0,0,0,0.2);color:#687E87 !important}#cw-uf_epicoepico_capture_widget .capture form .uf-email:focus,#cw-uf_epicoepico_capture_widget .capture form .uf-name:focus{box-shadow:0px 0px 10px 0px #ff5b5b;background-color:#FFFFFF;border:1px solid rgba(0,0,0,0.2)}#cw-uf_epicoepico_capture_widget .capture form .uf-submit{background-color:#ff5b5b !important;color:#FFFFFF}#cw-uf_epicoepico_capture_widget .capture form .uf-submit:hover{background-color:#ff2e00 !important}#cw-uf_epicoepico_capture_widget .uf-arrow svg polygon {fill:#0075bb !important}#cw-uf_epicoepico_capture_widget.ip.capture-wrap{border-bottom:10px solid #c6c6c6}#cw-uf_epicoepico_capture_widget .capture-inner:last-child{background-color:#0075bb}

ul.uf_epicoepico_links-list li a{background-color:#498ee3 !important;color:#FFFFFF!important}ul.uf_epicoepico_links-list li a:hover{background-color:#0099a7 !important}ul.uf_epicoepico_links-list li a:before{color: #FFFFFF !important}
ul.uf_epicoepico_links1-list li a{background-color:#498ee3 !important;color:#FFFFFF!important}ul.uf_epicoepico_links1-list li a:hover{background-color:#0099a7 !important}ul.uf_epicoepico_links1-list li a:before{color: #FFFFFF !important}
ul.uf_epicoepico_links2-list li a{background-color:#498ee3 !important;color:#FFFFFF!important}ul.uf_epicoepico_links2-list li a:hover{background-color:#0099a7 !important}ul.uf_epicoepico_links2-list li a:before{color: #FFFFFF !important}
ul.uf_epicoepico_links3-list li a{background-color:#498ee3 !important;color:#FFFFFF!important}ul.uf_epicoepico_links3-list li a:hover{background-color:#0099a7 !important}ul.uf_epicoepico_links3-list li a:before{color: #FFFFFF !important}

</style>
<link rel="stylesheet" id="epico-style-5-css" href="https://doutormultas.com.br/wp-content/themes/epico/css/color-styles/min/style5.min.css?ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="epico-fonts-css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,400i|Noto+Serif:400,700,400i,700i" type="text/css" media="all">
<link rel="stylesheet" id="bhittani_plugin_kksr-css" href="https://doutormultas.com.br/wp-content/plugins/kk-star-ratings/css.css?ver=2.5.1" type="text/css" media="all">
<link rel="stylesheet" id="wpProQuiz_front_style-css" href="https://doutormultas.com.br/wp-content/plugins/wp-pro-quiz/css/wpProQuiz_front.min.css?ver=0.37" type="text/css" media="all">
<link rel="stylesheet" id="sccss_style-css" href="https://doutormultas.com.br/?sccss=1&amp;ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="optimizepress-default-css" href="https://doutormultas.com.br/wp-content/plugins/optimizePressPlugin/lib/assets/default.min.css?ver=2.5.4.2" type="text/css" media="all">
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-includes/js/jquery/jquery.js?ver=1.12.4" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketoptimized="true">
/* <![CDATA[ */
var OptimizePress = {"ajaxurl":"https:\/\/doutormultas.com.br\/wp-admin\/admin-ajax.php","SN":"optimizepress","version":"2.5.4.2","script_debug":".min","localStorageEnabled":"","wp_admin_page":"","op_live_editor":"","op_page_builder":"","op_create_new_page":"","imgurl":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/lib\/images\/","OP_URL":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/","OP_JS":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/lib\/js\/","OP_PAGE_BUILDER_URL":"","include_url":"https:\/\/doutormultas.com.br\/wp-includes\/","op_autosave_interval":"300","op_autosave_enabled":"Y","paths":{"url":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/","img":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/lib\/images\/","js":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/lib\/js\/","css":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/lib\/css\/"},"social":{"twitter":"optimizepress","facebook":"optimizepress","googleplus":"111273444733787349971"},"flowplayerHTML5":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/lib\/modules\/blog\/video\/flowplayer\/flowplayer.swf","flowplayerKey":"","flowplayerLogo":"","mediaelementplayer":"https:\/\/doutormultas.com.br\/wp-content\/plugins\/optimizePressPlugin\/lib\/modules\/blog\/video\/mediaelement\/","pb_unload_alert":"This page is asking you to confirm that you want to leave - data you have entered may not be saved.","pb_save_alert":"Please make sure you are happy with all of your options as some options will not be able to be changed for this page later.","optimizemember":{"enabled":false,"version":"0"}};
/* ]]> */
</script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/plugins/optimizePressPlugin/lib/js/op-jquery-base-all.min.js?ver=2.5.4.2" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/plugins/kk-star-ratings/js.min.js?ver=2.5.1" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/plugins/duracelltomi-google-tag-manager/js/gtm4wp-form-move-tracker.js?ver=1.7.2" data-rocketoptimized="true"></script>
<link rel="https://api.w.org/" href="https://doutormultas.com.br/wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://doutormultas.com.br/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://doutormultas.com.br/wp-includes/wlwmanifest.xml">
<link rel="shortlink" href="https://doutormultas.com.br/?p=16917">
<link rel="alternate" type="application/json+oembed" href="https://doutormultas.com.br/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdoutormultas.com.br%2Fcalcdespesas%2F">
<link rel="alternate" type="text/xml+oembed" href="https://doutormultas.com.br/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdoutormultas.com.br%2Fcalcdespesas%2F&amp;format=xml">
<meta property="fb:pages" content=" 469176576571642">

<script type="text/rocketscript" data-rocketoptimized="true">
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:28137,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script><meta name="google-site-verification" content="W9iy301w1XDJxVpLEBiJwK2GhmnVvKR81F8v4R7cV3M">
<meta name="google-site-verification" content="W9iy301w1XDJxVpLEBiJwK2GhmnVvKR81F8v4R7cV3M"><style>.kk-star-ratings { width:120px; }.kk-star-ratings .kksr-stars a { width:24px; }.kk-star-ratings .kksr-stars, .kk-star-ratings .kksr-stars .kksr-fuel, .kk-star-ratings .kksr-stars a { height:24px; }.kk-star-ratings .kksr-star.gray { background-image: url(https://doutormultas.com.br/wp-content/plugins/kk-star-ratings/gray.png); }.kk-star-ratings .kksr-star.yellow { background-image: url(https://doutormultas.com.br/wp-content/plugins/kk-star-ratings/yellow.png); }.kk-star-ratings .kksr-star.orange { background-image: url(https://doutormultas.com.br/wp-content/plugins/kk-star-ratings/orange.png); }</style>

<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","mainEntityOfPage":{"@type":"WebPage","@id":"https:\/\/doutormultas.com.br\/calcdespesas\/"},"url":"https:\/\/doutormultas.com.br\/calcdespesas\/","headline":"Calculadora de despesas do carro","datePublished":"2018-02-15T11:25:32+00:00","dateModified":"2018-02-15T17:47:48+00:00","publisher":{"@type":"Organization","name":"Doutor Multas","logo":{"@type":"ImageObject","url":"https:\/\/doutormultas.com.br\/wp-content\/uploads\/2017\/02\/doutor-multas-logo.png","width":600,"height":60}},"author":{"@type":"Person","name":"Gustavo","url":"https:\/\/doutormultas.com.br\/author\/drgustavo\/","image":{"@type":"ImageObject","url":"https:\/\/secure.gravatar.com\/avatar\/a1730cfc9e470dfb6c439f6dbea5b970?s=96&d=mm&r=g","height":96,"width":96},"sameAs":["http:\/\/doutormultas.com.br","google.com\/+DoutormultasBr","https:\/\/www.facebook.com\/doutormultas"]}}</script>
<script data-cfasync="false" src="//load.sumome.com/" data-sumo-platform="wordpress" data-sumo-site-id="a8a4d1f6aa1bf62e4cc7a49a40f9f2e8eb64c61aca0a7743460fef548e29f3ce" async=""></script>

<script data-cfasync="false" type="text/javascript">
	var gtm4wp_datalayer_name = "dataLayer";
	var dataLayer = dataLayer || [];
	dataLayer.push({"pagePostType":"page","pagePostType2":"single-page","pagePostAuthor":"Gustavo"});
</script>
<script data-cfasync="false">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.'+'js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N834XP');</script>


<!--[if (gte IE 6)&(lte IE 8)]>
            <script type="text/javascript" src="https://doutormultas.com.br/wp-content/plugins/optimizePressPlugin/lib/js/selectivizr-1.0.2-min.js?ver=1.0.2"></script>
        <![endif]-->
<!--[if lt IE 9]>
            <script src="https://doutormultas.com.br/wp-content/plugins/optimizePressPlugin/lib/js//html5shiv.min.js"></script>
        <![endif]-->
<style type="text/css" id="custom-background-css">
body.custom-background { background-color: #ffffff; }
</style>
<link rel="icon" href="https://doutormultas.com.br/wp-content/uploads/2016/11/Untitled-3-1.png" sizes="32x32">
<link rel="icon" href="https://doutormultas.com.br/wp-content/uploads/2016/11/Untitled-3-1.png" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="https://doutormultas.com.br/wp-content/uploads/2016/11/Untitled-3-1.png">
<meta name="msapplication-TileImage" content="https://doutormultas.com.br/wp-content/uploads/2016/11/Untitled-3-1.png">
<style type="text/css">:root #header + #content > #left > #rlblock_left
{display:none !important;}</style><script data-rocketsrc="https://doutormultas.com.br/wp-includes/js/wp-emoji-release.min.js?ver=4.9.4" data-rocketoptimized="true" type="text/javascript" defer=""></script><script async="" data-rocketsrc="https://static.hotjar.com/c/hotjar-28137.js?sv=6" data-rocketoptimized="true"></script><script async="" type="text/javascript" data-rocketsrc="//doutomultas.disqus.com/count.js" data-rocketoptimized="true"></script><script async="" data-rocketsrc="https://my.hellobar.com/modules-4b7912ce300915f66efbf0b8a1493a792ccb85702da4f330ad8f3de5e8aa4566.js" data-rocketoptimized="true"></script><script async="" type="text/javascript" data-rocketsrc="//trackcmp.net/visit?actid=251746049&amp;e=&amp;r=&amp;u=https%3A%2F%2Fdoutormultas.com.br%2Fcalcdespesas%2F" data-rocketoptimized="true"></script><script type="text/javascript" src="https://trackcmp.net/visit?actid=251746049&amp;e=&amp;r=&amp;u=https%3A%2F%2Fdoutormultas.com.br%2Fcalcdespesas%2F"></script><script async="" data-rocketsrc="https://script.hotjar.com/modules-ccfc7a1f591f788c1e997b3c392cef07.js" data-rocketoptimized="true"></script><style type="text/css">iframe#_hjRemoteVarsFrame {display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;}</style><style type="text/css">#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container{margin:0;padding:0;border:none;overflow:hidden;position:absolute}body.logged-in.admin-bar #pull-down.hb-top-right,body.logged-in.admin-bar #pull-down.hb-top-left,body.logged-in.admin-bar #pull-down.hb-bar-top,body.logged-in.admin-bar #lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Slider.hb-top-right,body.logged-in.admin-bar #lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Slider.hb-top-left,body.logged-in.admin-bar #lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-bar-top{z-index:99998 !important;top:32px !important}.hellobar#pull-down.hb-animated{animation-duration:1s;-o-animation-duration:1s;-ms-animation-duration:1s;-moz-animation-duration:1s;-webkit-animation-duration:1s;animation-fill-mode:forwards;-o-animation-fill-mode:forwards;-ms-animation-fill-mode:forwards;-moz-animation-fill-mode:forwards;-webkit-animation-fill-mode:forwards}.hellobar#pull-down{top:-1px;right:10px;padding:3px;z-index:10000002;overflow:hidden;position:absolute;border-radius:0 0 5px 5px;transform:translateY(-40px);-o-transform:translateY(-40px);-ms-transform:translateY(-40px);-moz-transform:translateY(-40px);-webkit-transform:translateY(-40px)}.hellobar#pull-down .hellobar-arrow{cursor:pointer;display:flex;height:11px;opacity:0.3;width:11px;filter:flipv;transform:scaleY(-1);-o-transform:scaleY(-1);-ms-transform:scaleY(-1);-moz-transform:scaleY(-1);-webkit-transform:scaleY(-1)}.hellobar#pull-down .hellobar-arrow:hover{opacity:0.6}.hellobar#pull-down .hellobar-arrow svg{fill:white}.hellobar#pull-down.inverted .hellobar-arrow svg{fill:#3C3E3F}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.hb-animated{animation-duration:0.25s;-o-animation-duration:0.25s;-ms-animation-duration:0.25s;-moz-animation-duration:0.25s;-webkit-animation-duration:0.25s;animation-fill-mode:forwards;-o-animation-fill-mode:forwards;-ms-animation-fill-mode:forwards;-moz-animation-fill-mode:forwards;-webkit-animation-fill-mode:forwards}@keyframes hb-bounceInDown{0%,60%,75%,90%,100%{transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-o-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-ms-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-moz-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-webkit-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1)}0%{transform:translate3d(0, -3000px, 0);-o-transform:translate3d(0, -3000px, 0);-ms-transform:translate3d(0, -3000px, 0);-moz-transform:translate3d(0, -3000px, 0);-webkit-transform:translate3d(0, -3000px, 0)}60%{transform:translate3d(0, 0px, 0);-o-transform:translate3d(0, 0px, 0);-ms-transform:translate3d(0, 0px, 0);-moz-transform:translate3d(0, 0px, 0);-webkit-transform:translate3d(0, 0px, 0)}75%{transform:translate3d(0, -40px, 0);-o-transform:translate3d(0, -40px, 0);-ms-transform:translate3d(0, -40px, 0);-moz-transform:translate3d(0, -40px, 0);-webkit-transform:translate3d(0, -40px, 0)}90%{transform:translate3d(0, 0px, 0);-o-transform:translate3d(0, 0px, 0);-ms-transform:translate3d(0, 0px, 0);-moz-transform:translate3d(0, 0px, 0);-webkit-transform:translate3d(0, 0px, 0)}100%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}}@-moz-keyframes hb-bounceInDown{0%,60%,75%,90%,100%{-moz-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1)}0%{-moz-transform:translate3d(0, -3000px, 0)}60%{-moz-transform:translate3d(0, 0px, 0)}75%{-moz-transform:translate3d(0, -40px, 0)}90%{-moz-transform:translate3d(0, 0px, 0)}100%{-moz-transform:none}}@-webkit-keyframes hb-bounceInDown{0%,60%,75%,90%,100%{-webkit-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1)}0%{-webkit-transform:translate3d(0, -3000px, 0)}60%{-webkit-transform:translate3d(0, 0px, 0)}75%{-webkit-transform:translate3d(0, -40px, 0)}90%{-webkit-transform:translate3d(0, 0px, 0)}100%{-webkit-transform:none}}@keyframes hb-bounceInUp{0%,60%,75%,90%,100%{transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-o-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-ms-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-moz-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1);-webkit-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1)}0%{transform:translate3d(0, 3000px, 0);-o-transform:translate3d(0, 3000px, 0);-ms-transform:translate3d(0, 3000px, 0);-moz-transform:translate3d(0, 3000px, 0);-webkit-transform:translate3d(0, 3000px, 0)}60%{transform:translate3d(0, 0, 0);-o-transform:translate3d(0, 0, 0);-ms-transform:translate3d(0, 0, 0);-moz-transform:translate3d(0, 0, 0);-webkit-transform:translate3d(0, 0, 0)}75%{transform:translate3d(0, 40px, 0);-o-transform:translate3d(0, 40px, 0);-ms-transform:translate3d(0, 40px, 0);-moz-transform:translate3d(0, 40px, 0);-webkit-transform:translate3d(0, 40px, 0)}90%{transform:translate3d(0, 0px, 0);-o-transform:translate3d(0, 0px, 0);-ms-transform:translate3d(0, 0px, 0);-moz-transform:translate3d(0, 0px, 0);-webkit-transform:translate3d(0, 0px, 0)}100%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}}@-moz-keyframes hb-bounceInUp{0%,60%,75%,90%,100%{-moz-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1)}0%{-moz-transform:translate3d(0, 3000px, 0)}60%{-moz-transform:translate3d(0, 0, 0)}75%{-moz-transform:translate3d(0, 40px, 0)}90%{-moz-transform:translate3d(0, 0, 0)}100%{-moz-transform:none}}@-webkit-keyframes hb-bounceInUp{0%,60%,75%,90%,100%{-webkit-transition-timing-function:cubic-bezier(0.215, 0.61, 0.355, 1)}0%{-webkit-transform:translate3d(0, 3000px, 0)}60%{-webkit-transform:translate3d(0, 0px, 0)}75%{-webkit-transform:translate3d(0, 40px, 0)}90%{-webkit-transform:translate3d(0, 0px, 0)}100%{-webkit-transform:none}}@keyframes hb-bounceOutUp{0%,100%{transition-timing-function:ease-in;-o-transition-timing-function:ease-in;-ms-transition-timing-function:ease-in;-moz-transition-timing-function:ease-in;-webkit-transition-timing-function:ease-in}0%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}100%{transform:translate3d(0, -500px, 0);-o-transform:translate3d(0, -500px, 0);-ms-transform:translate3d(0, -500px, 0);-moz-transform:translate3d(0, -500px, 0);-webkit-transform:translate3d(0, -500px, 0)}}@-moz-keyframes hb-bounceOutUp{0%,100%{-moz-transition-timing-function:ease-in}0%{-moz-transform:none}100%{-moz-transform:translate3d(0, -500px, 0)}}@-webkit-keyframes hb-bounceOutUp{0%,100%{-webkit-transition-timing-function:ease-in}0%{-webkit-transform:none}100%{-webkit-transform:translate3d(0, -500px, 0)}}@keyframes hb-bounceOutDown{0%,100%{transition-timing-function:ease-in;-o-transition-timing-function:ease-in;-ms-transition-timing-function:ease-in;-moz-transition-timing-function:ease-in;-webkit-transition-timing-function:ease-in}0%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}100%{transform:translate3d(0, 500px, 0);-o-transform:translate3d(0, 500px, 0);-ms-transform:translate3d(0, 500px, 0);-moz-transform:translate3d(0, 500px, 0);-webkit-transform:translate3d(0, 500px, 0)}}@-moz-keyframes hb-bounceOutDown{0%,100%{-moz-transition-timing-function:ease-in}0%{-moz-transform:none}100%{-moz-transform:translate3d(0, 500px, 0)}}@-webkit-keyframes hb-bounceOutDown{0%,100%{-webkit-transition-timing-function:ease-in}0%{-webkit-transform:none}100%{-webkit-transform:translate3d(0, 500px, 0)}}@keyframes hb-bounceInLeft{0%,75%,100%{transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-o-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-ms-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{transform:translate3d(200%, 0, 0);-o-transform:translate3d(200%, 0, 0);-ms-transform:translate3d(200%, 0, 0);-moz-transform:translate3d(200%, 0, 0);-webkit-transform:translate3d(200%, 0, 0)}75%{transform:translate3d(-10%, 0, 0);-o-transform:translate3d(-10%, 0, 0);-ms-transform:translate3d(-10%, 0, 0);-moz-transform:translate3d(-10%, 0, 0);-webkit-transform:translate3d(-10%, 0, 0)}100%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}}@-moz-keyframes hb-bounceInLeft{0%,75%,100%{-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-moz-transform:translate3d(200%, 0, 0)}75%{-moz-transform:translate3d(-10%, 0, 0)}100%{-moz-transform:none}}@-webkit-keyframes hb-bounceInLeft{0%,75%,100%{-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-webkit-transform:translate3d(200%, 0, 0)}75%{-webkit-transform:translate3d(-10%, 0, 0)}100%{-webkit-transform:none}}@keyframes hb-bounceOutRight{0%,25%,100%{transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-o-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-ms-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}25%{transform:translate3d(-10%, 0, 0);-o-transform:translate3d(-10%, 0, 0);-ms-transform:translate3d(-10%, 0, 0);-moz-transform:translate3d(-10%, 0, 0);-webkit-transform:translate3d(-10%, 0, 0)}100%{transform:translate3d(200%, 0, 0);-o-transform:translate3d(200%, 0, 0);-ms-transform:translate3d(200%, 0, 0);-moz-transform:translate3d(200%, 0, 0);-webkit-transform:translate3d(200%, 0, 0)}}@-moz-keyframes hb-bounceOutRight{0%,25%,100%{-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-moz-transform:none}25%{-moz-transform:translate3d(-10%, 0, 0)}100%{-moz-transform:translate3d(200%, 0, 0)}}@-webkit-keyframes hb-bounceOutRight{0%,25%,100%{-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-webkit-transform:none}25%{-webkit-transform:translate3d(-10%, 0, 0)}100%{-webkit-transform:translate3d(200%, 0, 0)}}@keyframes hb-bounceInRight{0%,75%,100%{transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-o-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-ms-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{transform:translate3d(-200%, 0, 0);-o-transform:translate3d(-200%, 0, 0);-ms-transform:translate3d(-200%, 0, 0);-moz-transform:translate3d(-200%, 0, 0);-webkit-transform:translate3d(-200%, 0, 0)}75%{transform:translate3d(10%, 0, 0);-o-transform:translate3d(10%, 0, 0);-ms-transform:translate3d(10%, 0, 0);-moz-transform:translate3d(10%, 0, 0);-webkit-transform:translate3d(10%, 0, 0)}100%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}}@-moz-keyframes hb-bounceInRight{0%,75%,100%{-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-moz-transform:translate3d(-200%, 0, 0)}75%{-moz-transform:translate3d(10%, 0, 0)}100%{-moz-transform:none}}@-webkit-keyframes hb-bounceInRight{0%,75%,100%{-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-webkit-transform:translate3d(-200%, 0, 0)}75%{-webkit-transform:translate3d(10%, 0, 0)}100%{-webkit-transform:none}}@keyframes hb-bounceOutLeft{0%,25%,100%{transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-o-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-ms-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255);-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}25%{transform:translate3d(10%, 0, 0);-o-transform:translate3d(10%, 0, 0);-ms-transform:translate3d(10%, 0, 0);-moz-transform:translate3d(10%, 0, 0);-webkit-transform:translate3d(10%, 0, 0)}100%{transform:translate3d(-200%, 0, 0);-o-transform:translate3d(-200%, 0, 0);-ms-transform:translate3d(-200%, 0, 0);-moz-transform:translate3d(-200%, 0, 0);-webkit-transform:translate3d(-200%, 0, 0)}}@-moz-keyframes hb-bounceOutLeft{0%,25%,100%{-moz-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-moz-transform:none}25%{-moz-transform:translate3d(10%, 0, 0)}100%{-moz-transform:translate3d(-200%, 0, 0)}}@-webkit-keyframes hb-bounceOutLeft{0%,25%,100%{-webkit-transition-timing-function:cubic-bezier(0.65, -0.25, 0.325, 1.255)}0%{-webkit-transform:none}25%{-webkit-transform:translate3d(10%, 0, 0)}100%{-webkit-transform:translate3d(-200%, 0, 0)}}@keyframes hb-fadeIn{0%,100%{transition-timing-function:ease-in;-o-transition-timing-function:ease-in;-ms-transition-timing-function:ease-in;-moz-transition-timing-function:ease-in;-webkit-transition-timing-function:ease-in}0%{opacity:0}100%{opacity:1}}@-moz-keyframes hb-fadeIn{0%,100%{-moz-transition-timing-function:ease-in}0%{opacity:0}100%{opacity:1}}@-webkit-keyframes hb-fadeIn{0%,100%{-webkit-transition-timing-function:ease-in}0%{opacity:0}100%{opacity:1}}@keyframes hb-fadeOut{0%,100%{transition-timing-function:ease-in;-o-transition-timing-function:ease-in;-ms-transition-timing-function:ease-in;-moz-transition-timing-function:ease-in;-webkit-transition-timing-function:ease-in}0%{opacity:1}100%{opacity:0}}@-moz-keyframes hb-fadeOut{0%,100%{-moz-transition-timing-function:ease-in}0%{opacity:1}100%{opacity:0}}@-webkit-keyframes hb-fadeOut{0%,100%{-webkit-transition-timing-function:ease-in}0%{opacity:1}100%{opacity:0}}@keyframes hb-fadeInDown{0%,100%{transition-timing-function:ease-in;-o-transition-timing-function:ease-in;-ms-transition-timing-function:ease-in;-moz-transition-timing-function:ease-in;-webkit-transition-timing-function:ease-in}0%{opacity:0;height:110%;margin-top:-5%}100%{opacity:1;height:100%;margin-top:0}}@-moz-keyframes hb-fadeInDown{0%,100%{-moz-transition-timing-function:ease-in}0%{opacity:0;height:110%;margin-top:-5%}100%{opacity:1;height:100%;margin-top:0}}@-webkit-keyframes hb-fadeInDown{0%,100%{-webkit-transition-timing-function:ease-in}0%{opacity:0;height:110%;margin-top:-5%}100%{opacity:1;height:100%;margin-top:0}}@keyframes hb-fadeOutUp{0%,100%{transition-timing-function:ease-in;-o-transition-timing-function:ease-in;-ms-transition-timing-function:ease-in;-moz-transition-timing-function:ease-in;-webkit-transition-timing-function:ease-in}0%{opacity:1;height:100%;margin-top:0}100%{opacity:0;height:110%;margin-top:-5%}}@-moz-keyframes hb-fadeOutUp{0%,100%{-moz-transition-timing-function:ease-in}0%{opacity:1;height:100%;margin-top:0}100%{opacity:0;height:110%;margin-top:-5%}}@-webkit-keyframes hb-fadeOutUp{0%,100%{-webkit-transition-timing-function:ease-in}0%{opacity:1;height:100%;margin-top:0}100%{opacity:0;height:110%;margin-top:-5%}}

#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar{top:0;left:0;width:100%;min-height:40px;max-height:40px;z-index:10000001}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-large{min-height:50px;max-height:50px}#hellobar-pusher{height:30px;overflow:hidden;position:relative;flex:none}#hellobar-pusher.hb-large{height:50px}.hellobar#pull-down.hb-bar-bottom{top:auto;bottom:-1px;position:fixed;border-radius:5px 5px 0 0;transform:translateY(40px);-o-transform:translateY(40px);-ms-transform:translateY(40px);-moz-transform:translateY(40px);-webkit-transform:translateY(40px)}.hellobar#pull-down.hb-bar-bottom .hellobar-arrow{filter:none;transform:none;-o-transform:none;-ms-transform:none;-moz-transform:none;-webkit-transform:none}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.remains-in-place{top:0;position:fixed;_position:absolute;_top:expression(eval(document.body.scrollTop))}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-bar-bottom{top:auto;bottom:0;position:fixed;_position:absolute;_bottom:expression(eval(document.body.scrollTop))}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-animated{animation-duration:1s;-o-animation-duration:1s;-ms-animation-duration:1s;-moz-animation-duration:1s;-webkit-animation-duration:1s}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-animateIn,.hellobar#pull-down.hb-animateIn{animation-name:hb-bounceInDown;-o-animation-name:hb-bounceInDown;-ms-animation-name:hb-bounceInDown;-moz-animation-name:hb-bounceInDown;-webkit-animation-name:hb-bounceInDown}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-animateOut,.hellobar#pull-down.hb-animateOut{animation-name:hb-bounceOutUp;-o-animation-name:hb-bounceOutUp;-ms-animation-name:hb-bounceOutUp;-moz-animation-name:hb-bounceOutUp;-webkit-animation-name:hb-bounceOutUp}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-animateIn.hb-bar-bottom,.hellobar#pull-down.hb-animateIn.hb-bar-bottom{animation-name:hb-bounceInUp;-o-animation-name:hb-bounceInUp;-ms-animation-name:hb-bounceInUp;-moz-animation-name:hb-bounceInUp;-webkit-animation-name:hb-bounceInUp}#lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container.HB-Bar.hb-animateOut.hb-bar-bottom,.hellobar#pull-down.hb-animateOut.hb-bar-bottom{animation-name:hb-bounceOutDown;-o-animation-name:hb-bounceOutDown;-ms-animation-name:hb-bounceOutDown;-moz-animation-name:hb-bounceOutDown;-webkit-animation-name:hb-bounceOutDown}

</style><style type="text/css">.fancybox-margin{margin-right:14px;}</style><link type="text/css" rel="stylesheet" href="https://pushcrew.com/http-v4/css/httpFront-v4.css"></head>
<body class="wordpress ltr pt pt-br child-theme y2018 m02 d23 h08 friday logged-out custom-background singular singular-page singular-page-16917 page-template-default op-plugin epico-sidebar-left epc-s5 epc-nsc epc-ss epc-sst epico-compact epc-meta-icons epc-mobcol" dir="ltr" itemscope="itemscope" itemtype="http://schema.org/WebPage" style="padding-top: 0px;">
<iframe src="about:blank" id="lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container" class="HB-Bar blue-autumn hb-66 hb-bar-top" name="lf291aac3e0b9e09b0a56b3b6f26f49517bf121d7-container-0" scrolling="no" frameborder="0" style="display: inline; max-height: 74px;"></iframe><div id="hellobar-pusher" class="hb-66" style="height: 66px;"></div><div id="page">
<header id="header" class="site-header" role="banner" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="wrap">
<div id="branding">
<p id="site-title" itemscope="" itemtype="http://schema.org/Organization">
<a itemprop="url" href="https://doutormultas.com.br" rel="home" title="Homepage" class="img-hyperlink">
<meta itemprop="name" content="">
<img style="width: 172px" id="logo" itemprop="image logo" src="https://doutormultas.com.br/wp-content/uploads/2018/02/doutor-multas-logotipo.png" alt="">
</a>
</p>
</div>
<div class="nav" id="nav">

<nav class="menu menu-primary" role="navigation" id="menu-primary" aria-label="Navegação primária Menu" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<a id="nav-toggle" href="#" title="Alternar navegação"><span class="screen-reader-text">Alternar navegação</span><span class="nav-text">Menu</span></a>
<div class="assistive-text skip-link">
<a href="#content">Pular para o conteúdo</a>
</div>
<ul id="menu-primary-items" class="menu-items"><li id="menu-item-20" class="fa fa-home menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-20"><a href="http://doutormultas.com.br/">Início</a></li>
<li id="menu-item-3146" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3146"><a href="http://doutormultas.com.br/sobre">Sobre</a></li>
<li id="menu-item-2856" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2856"><a href="http://doutormultas.com.br/como-recorrer-uma-multa-de-transito">Como Recorrer</a></li>
<li id="menu-item-2928" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2928"><a href="https://doutormultas.com.br/depoimentos">Depoimentos</a></li>
<li id="menu-item-11718" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-has-children menu-item-11718"><a href="https://doutormultas.com.br/">Categorias</a>
<ul class="sub-menu">
<li id="menu-item-11719" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11719"><a href="https://doutormultas.com.br/category/multas/">Multas</a></li>
<li id="menu-item-11721" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11721"><a href="https://doutormultas.com.br/category/excesso-de-velocidade/">Excesso de Velocidade</a></li>
<li id="menu-item-11720" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11720"><a href="https://doutormultas.com.br/category/lei-seca/">Lei Seca</a></li>
<li id="menu-item-11725" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11725"><a href="https://doutormultas.com.br/category/suspensao-da-cnh/">Suspensão da CNH</a></li>
<li id="menu-item-11722" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11722"><a href="https://doutormultas.com.br/category/cassacao-da-cnh/">Cassação da CNH</a></li>
<li id="menu-item-11723" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11723"><a href="https://doutormultas.com.br/category/codigo-de-transito-brasileiro/">Código de Trânsito Brasileiro</a></li>
<li id="menu-item-11724" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11724"><a href="https://doutormultas.com.br/category/dicas-transito/">Dicas de Trânsito</a></li>
</ul>
</li>
<li id="menu-item-17" class="fa fa-envelope-o menu-item menu-item-type-post_type menu-item-object-page menu-item-17 menu-item-parent-archive"><a href="https://doutormultas.com.br/contato/">Contato</a></li>
</ul>
</nav>
<div id="search-wrap">
<a id="search-toggle" href="#" title="Busca"><span class="search-text">Busca</span></a>
<form role="search" method="get" class="search-form" action="https://doutormultas.com.br/" style="display: none;">
<input type="search" placeholder=" Buscar por:" name="s" class="search-field fa" value="">
<input class="search-submit fa fa-search" type="submit" value="Ok ">
</form>
</div>
</div>
</div>
</header>
<aside class="sidebar sidebar-promo sidebar-col-1" role="complementary" id="sidebar-promo" aria-label="Promocional Barra lateral" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
<section id="epico_capture_widget-id-12" class="widget widget_epico_capture_widget-id"><!--[if IE]><div id="cw-uf_epicoepico_capture_widget"  class="capture-wrap uf-wrap ip fw ie"><![endif]--><!--[if !IE]><!--><div id="cw-uf_epicoepico_capture_widget" class="capture-wrap uf-wrap ip fw"><!--<![endif]--><div class="capture capture-inner uf uf-inner "><div class="capture-container uf-container "><p itemprop="text" class="capture-notice uf-notice ">Como recorrer de multas de trânsito em apenas 3 passos</p><p class="capture-iconinner uf-iconinner"><img src="https://doutormultas.com.br/wp-content/uploads/2017/08/ebookcircular4.png" class="icon"></p><form id="uf_epicoepico_capture_widget" method="post" enctype="multipart/form-data" action="https://doutormultas.activehosted.com/proc.php" accept-charset="utf-8" target="_blank" rel="noopener noreferrer" novalidate="novalidate">
<input type="hidden" name="u" value="45">
<input type="hidden" name="f" value="45">
<input type="hidden" name="s">
<input type="hidden" name="c" value="0">
<input type="hidden" name="m" value="0">
<input type="hidden" name="act" value="sub">
<input type="hidden" name="v" value="2"><input class="uf-email " type="email" name="email" placeholder=" Insira seu melhor email aqui">
<span class="capture-wrapicon uf-wrapicon">
<input class="uf-submit" name="uf-submit" value="Quero baixar agora" type="submit">
</span>
</form></div></div><script type="text/rocketscript" data-rocketoptimized="true">var uf_widget_notice = document.getElementById('cw-uf_epicoepico_capture_widget' );uf_widget_notice.insertAdjacentHTML('beforeend', '<div style="display: none" class="capture-overlay uf-overlay"><div class="capture-overlay-warning uf-overlay-warning">Esta área é muito estreita para esta versão do widget. Transfira-o para uma área mais larga ou ative a versão para a barra lateral. <a class="capture-link uf-link" href="https://doutormultas.com.br/wp-admin/customize.php">nas opções do widget</a>, dentro da aba "Personalização".</div></div>' );</script><div style="display: none" class="capture-overlay uf-overlay"><div class="capture-overlay-warning uf-overlay-warning">Esta área é muito estreita para esta versão do widget. Transfira-o para uma área mais larga ou ative a versão para a barra lateral. <a class="capture-link uf-link" href="https://doutormultas.com.br/wp-admin/customize.php">nas opções do widget</a>, dentro da aba "Personalização".</div></div></div></section>
</aside>
<div class="augesystems">
<div id="faixa-divisao" class="container-fluid screen-only">
</div>
<div class="container">
<div id="title">
<h1>Simulador de financiamento</h1>
<h2 class="print-only">www.doutormultas.com.br</h2>
<h2 class="screen-only">Preencha os campos abaixo e calcule o valor total do veículo.</h2>
</div>



<form method="POST" action="teste.php">

<div id="selveiculo" class="row">
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
<div class="col-md-12">
<div class="input-group">
<label for="i_carro_valor" class="right" >Valor do veículo</label>
<input type="text" id="valor" for="valor" required="" name="valor" placeholder="Valor de venda">
<h6 style="margin: 0px"  >Não sabe o valor?</h6>
<div class="row screen-only">
<div style="margin-top: -20px" id="consulta_fipe_link" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0">
<a href="https://doutormultas.com.br/fipe/">Consultar tabela FIPE</a>
</div> 
</div>
</div>

<div class="input-group">
<label for="i_carro_valor">Taxa de Juros</label>
<input type="text" id="i_carro_valor" required="" step="1" name="juros"  placeholder="% a.m">
<label for="i_carro_valor">Valor da entrada</label>
<input type="text" id="i_carro_valor" required="" name="entrada" step="1" placeholder="Entrada">
<label for="i_carro_valor">Meses</label>
<input type="text" id="i_carro_valor" required="" name="meses" step="1" placeholder="Nº de vezes">
<button id="processar" type="submit">Calcular</button> <br>

</form>

<?php



if (!empty($_POST["valor"])) {
 $valor = $_POST["valor"];
 $valor = str_replace(".","",$valor);
$valor = str_replace(",",".",$valor);


$juros = $_POST["juros"];
$meses = $_POST["meses"];

$entrada = $_POST["entrada"];
$conta_valor = $valor - $entrada; 
$conta_entrada = $valor - $conta_valor;  
$conta_taxa = ($juros / 100); 

$conta = pow((1 + $conta_taxa), $meses);
$conta = (1 / $conta);
$conta = (1 - $conta);
$conta = ($conta_taxa / $conta);
$parcela = ($conta_valor * $conta);
$total_geral = $conta_entrada + ($parcela * $meses);

$parcela = number_format($parcela, 2, ',', '.');
$valor_entrada = number_format($conta_entrada, 2, ',', '.');
$total_geral = number_format($total_geral, 2, ',', '.');
$conta_valor = number_format($conta_valor, 2, ',', '.');
$juros = ($conta_taxa * 100);
$juros = number_format($juros, 2, ',', '.');

echo "
Entrada de <b>R$ $valor_entrada</b><BR>
 + <b>$meses</b> parcelas de <b>R$ $parcela</b> <BR>
 A uma taxa de <b>$juros%</b> a.m.<BR>
 Valor total será de <b>R$ $total_geral</b>";
}


?>


</div>
</div>
</div>



<div id="dados_veiculo" class="row" style="display: none;">
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
<h2>Resultado</h2>
<div class="row">
<div class="col-md-4">
<div class="col-md-12"><span>Por dia</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_dia">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por mês</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_mes">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por ano</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_ano">R$ 0,00</span></div>
</div>
</div>
<h2>Gastos considerando desvalorizaçao</h2>
<div class="row">
<div class="col-md-4">
<div class="col-md-12"><span>Por dia</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_dia">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por mês</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_mes">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por ano</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_ano">R$ 0,00</span></div>
</div>
</div>
<div class="row">
<button id="btn_imprimir">Imprimir</button>
</div>
</div>
</div>
</div>
</div>
<footer id="footer" class="site-footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
<div id="credits">
<div class="wrap">
<div class="credit">
<p>
<span id="credit-text"><a href="https://doutormultas.com.br" rel="home" title=""></a> · 2018
© Doutor Multas - CNPJ: 24.315.300/0001-28 | Escritório: World Trade Center – 9º Andar - Av. das Nações Unidas, 12551 - São Paulo | Escritório (Sede): Rua Pinto Martins 79, Pelotas/RS |
Atendemos todo o Brasil | Telefones: (53) 33074581 |
E-mail para contato: doutormultas@doutormultas.com.br |
O Doutor Multas não presta qualquer serviço restritivo de advogado ou outro tipo de serviço jurídico, atuando apenas na esfera administrativa.
</span>
</p>

<nav class="menu menu-secondary" role="navigation" id="menu-secondary" aria-label="Links do rodapé Menu" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<ul id="menu-secondary-items" class="menu-items"><li id="menu-item-2887" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2887 menu-item-parent-archive"><a href="https://doutormultas.com.br/aviso-legal/">Aviso Legal</a></li>
<li id="menu-item-2888" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2888 menu-item-parent-archive"><a href="https://doutormultas.com.br/politica-de-privacidade/">Política de Privacidade</a></li>
<li id="menu-item-2923" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2923 menu-item-parent-archive"><a href="https://doutormultas.com.br/termos-e-condicoes/">Termos e Condições</a></li>
<li id="menu-item-12550" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12550 menu-item-parent-archive"><a href="https://doutormultas.com.br/faq/">FAQ – Perguntas Frequentes | Doutor Multas</a></li>
</ul>
</nav>
</div>
</div>
</div>
</footer>
</div>
<script data-cfasync="false" src="/cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script><script type="text/rocketscript" data-rocketoptimized="true">
        // <![CDATA[
        var disqus_shortname = 'doutomultas';
        (function () {
            var nodes = document.getElementsByTagName('span');
            for (var i = 0, url; i < nodes.length; i++) {
                if (nodes[i].className.indexOf('dsq-postid') != -1 && nodes[i].parentNode.tagName == 'A') {
                    nodes[i].parentNode.setAttribute('data-disqus-identifier', nodes[i].getAttribute('data-dsqidentifier'));
                    url = nodes[i].parentNode.href.split('#', 1);
                    if (url.length == 1) { url = url[0]; }
                    else { url = url[1]; }
                    nodes[i].parentNode.href = url + '#disqus_thread';
                }
            }
            var s = document.createElement('script');
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
        }());
        // ]]>
        </script>
<script data-rocketsrc="//my.hellobar.com/6399785b2927aac956695146648d2e388a4b70ed.js" type="text/rocketscript" async="async" data-rocketoptimized="true"></script>

<noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-N834XP"
height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript>
<script type="text/rocketscript" data-rocketoptimized="true">
(function ($) {
	"use strict";

	$(function () {
		var $capture = $( '#cw-uf_epicoepico_capture_widget' );
		var $capture_sidebar = $( '#after-primary' ).find( '#cw-uf_epicoepico_capture_widget' ).last();

		if ( $( '#cw-uf_epicoepico_capture_widget:visible' ).length ){
			$( window ).on( 'resize',function() {
				if( $( window ).width() > 700 && $( '#cw-uf_epicoepico_capture_widget.fw' ).width() < 550){
					$( '#cw-uf_epicoepico_capture_widget.fw' ).css( 'min-height','500px' ).addClass( 'ol' );
					$( '#cw-uf_epicoepico_capture_widget.fw .capture-overlay' ).show();
				} else {
					$( '#cw-uf_epicoepico_capture_widget .capture-overlay' ).hide();
				}
			}).trigger( 'resize' );
		}

	
		});
}(jQuery));
</script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/plugins/uf-epico/assets/js/capture-plugin.js?ver=4.9.4" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketoptimized="true">
/* <![CDATA[ */
var php_data = {"ac_settings":{"tracking_actid":251746049,"site_tracking":1},"user_email":""};
/* ]]> */
</script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/plugins/activecampaign-subscription-forms/site_tracking.js?ver=4.9.4" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/themes/epico-jr/js/augesystems_functions.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/themes/epico-jr/js/gastoscalc.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketoptimized="true">
/* <![CDATA[ */
var epico_script_vars = {"fb_app_id":"app_id","fb_app_secret":"app_secret","fb_app_fields":"share","fb_app_version":"v2.8"};
/* ]]> */
</script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/themes/epico/js/scripts.min.js" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketoptimized="true">
/* <![CDATA[ */
var tve_dash_front = {"ajaxurl":"https:\/\/doutormultas.com.br\/wp-admin\/admin-ajax.php","is_crawler":""};
/* ]]> */
</script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/plugins/thrive-leads/thrive-dashboard/js/dist/frontend.min.js?ver=2.0.17" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-content/plugins/q2w3-fixed-widget/js/q2w3-fixed-widget.min.js?ver=5.0.4" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketsrc="https://doutormultas.com.br/wp-includes/js/wp-embed.min.js?ver=4.9.4" data-rocketoptimized="true"></script>
<script type="text/rocketscript" data-rocketoptimized="true">/*<![CDATA[*/if ( !window.TL_Const ) var TL_Const={"security":"d5c7679297","ajax_url":"https:\/\/doutormultas.com.br\/wp-admin\/admin-ajax.php","forms":[],"action_conversion":"tve_leads_ajax_conversion","action_impression":"tve_leads_ajax_impression","ajax_load":0,"main_group_id":11329,"display_options":{"allowed_post_types":[],"flag_url_match":null},"custom_post_data":[],"current_screen":{"screen_type":3,"screen_id":16917},"ignored_fields":["email","_captcha_size","_captcha_theme","_captcha_type","_submit_option","_use_captcha","g-recaptcha-response","__tcb_lg_fc","__tcb_lg_msg","_state","_form_type","_error_message_option","_back_url","_submit_option","url","_asset_group","_asset_option","mailchimp_optin"]}/*]]> */</script>


<iframe name="_hjRemoteVarsFrame" title="_hjRemoteVarsFrame" id="_hjRemoteVarsFrame" src="https://vars.hotjar.com/rcj-99d43ead6bdf30da8ed5ffcb4f17100c.html" style="display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;"></iframe><style id="extraClass">.extraClassAspect {-webkit-transform: scaleX(1)!important;}.extraClassCrop {-webkit-transform: scale(1)!important;}</style></body></html>