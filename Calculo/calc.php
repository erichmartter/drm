<HTML>
<HEAD>
 <TITLE>Simulador de Financiamento</TITLE>
<style type="text/css">
body {background-color: #2c2c2c; font-family: Arial, Tahoma, sans-serif; font-size:12px; color:#fff;}
.borda {
border-width:1px;
border-style: solid;
border-color: #666;
}
</style>
</HEAD>
<BODY>

<form method="POST" action="calc.php">
<h3>SIMULADOR DE FINANCIAMENTO</h3>
<table border="0" width="500" cellpadding=4 cellspacing=2>
<tr>
  <td width="40%" align="right" class="borda">Veículo:</td>
  <td width="60%" align="left" class="borda"><input type="text" size="20" value="Teste" name="veiculo"></td>
</tr>
<tr>
  <td width="40%" align="right" class="borda">Valor do Veículo:</td>
  <td width="60%" align="left" class="borda"><input type="text" size="20" value="20000" name="valor"></td>
</tr>
<tr>
  <td width="40%" align="right" class="borda">Taxa de Juros:</td>
  <td width="60%" align="left" class="borda"><input type="text" size="20" value="2" name="juros"></td>
</tr>
<tr>
  <td align="right" class="borda">Valor da Entrada:</td>
  <td width="60%" align="left" class="borda"><input type="number_format" size="20" value="2" name="entrada"></td>  
</tr>
<tr>
  <td align="right" class="borda">Meses:</td>
  <td align="left" class="borda"><select name="meses">
    <option value="0">Selecione</option>
    <option value="6">06 Meses</option>
    <option value="12">12 Meses</option>
    <option value="18">18 Meses</option>
    <option value="24" selected="">24 Meses</option>
    <option value="36">36 Meses</option>
    <option value="48">48 Meses</option>S
  </select>
</td>
</tr>

<tr>
  <td align="right" class="borda">&nbsp;</td>
  <td align="left" class="borda"><input type="submit" value="Enviar"></td>
</tr>
</table>

</form>


<?php



if (!empty($_POST["valor"])) {
 $valor = $_POST["valor"];
 $valor = str_replace(".","",$valor);
$valor = str_replace(",",".",$valor);


$juros = $_POST["juros"];
$meses = $_POST["meses"];

$entrada = $_POST["entrada"];
$conta_valor = $valor - $entrada; 
$conta_entrada = $valor - $conta_valor;  
$conta_taxa = ($juros / 100); 

$conta = pow((1 + $conta_taxa), $meses);
$conta = (1 / $conta);
$conta = (1 - $conta);
$conta = ($conta_taxa / $conta);
$parcela = ($conta_valor * $conta);
$total_geral = $conta_entrada + ($parcela * $meses);

$parcela = number_format($parcela, 2, ',', '.');
$valor_entrada = number_format($conta_entrada, 2, ',', '.');
$total_geral = number_format($total_geral, 2, ',', '.');
$conta_valor = number_format($conta_valor, 2, ',', '.');
$juros = ($conta_taxa * 100);
$juros = number_format($juros, 2, ',', '.');

echo "
Entrada de <b>R$ $valor_entrada</b><BR>
 + <b>$meses</b> parcelas de <b>R$ $parcela</b> <BR>
 A uma taxa de <b>$juros%</b> a.m.<BR>
 Valor total será de <b>R$ $total_geral</b>";
}


?>

</BODY>
</HTML>