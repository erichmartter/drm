<html lang="pt-BR" prefix="og: http://ogp.me/ns#" class="no-js">
     <?PHP 
header( 'Expires: Sat, 26 Jul 1997 05:00:00 GMT' ); 
header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' ); 
header( 'Cache-Control: no-store, no-cache, must-revalidate' ); 
header( 'Cache-Control: post-check=0, pre-check=0', false ); 
header( 'Pragma: no-cache' ); 

?>
    <head>
        <title>Calculadora consumo de combustível · Doutor Multas</title>
        <meta charset="utf-8">
        <link rel="stylesheet" id="bootstrap-css" href="https://doutormultas.com.br/wp-content/themes/epico-jr/bootstrap/css/bootstrap.min.css?ver=4.9.4" type="text/css" media="all">
        <link rel="stylesheet" id="style-css" href="https://doutormultas.com.br/wp-content/themes/epico-jr/style.css?ver=4.9.4" type="text/css" media="all">
        <link rel="stylesheet" id="parent-css" href="https://doutormultas.com.br/wp-content/themes/epico/style.min.css?ver=4.9.4" type="text/css" media="all">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
       

        <script>
            function somenteNumeros(num) {
                var er = /[^0-9.,]/;
                er.lastIndex = 0;
                var campo = num;
                if (er.test(campo.value)) {
                    campo.value = "";
                }
            }
        </script>
    </head>


    <body style="background: white">
    <div  class="augesystems">

        <div class="container">
            <div id="title">
                <h1>Calculadora de consumo de combustível</h1>
                <h2 class="print-only">www.doutormultas.com.br</h2>
                <h2 class="screen-only">Preencha os campos abaixo e calcule o consumo do seu veículo.</h2>
            </div>



          
                <div id="selveiculo" class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                        <div class="col-md-12">
                            <div class="input-group">
                                <label for="km">Qual a quilometragem?</label>
                                <input type="text" class="somente-numero" onkeyup="somenteNumeros(this);" id="km" required="" step="1" name="km" placeholder="Ex.: 450">
                                <label for="litros">Quantos litros você abasteceu?</label>
                                <input type="text" class="somente-numero" onkeyup="somenteNumeros(this);" id="litros" required="" name="litros" step="2" placeholder="Ex.: 39">
                                <button id="processar" type="submit">Calcular</button> <br>
                            </div>
                        </div>
                    </div>
                </div>
            
            
            	<div id="dados_veiculo" class="row">
                    	<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
				<h2>Resultado do cálculo</h2>
				
                                <div class="row">
                                    <div class="col-md-12"><span class="dados_result" id="resultado">Seu veículo fez uma média de $resultado quilômetros por litro.</span></div>
				</div>
				
                                <div class="row">
					<button id="btn_imprimir">Imprimir</button>
				</div>
			</div> 
		</div>
            
            
            
            
            
         </div>
    </div>
        </body>
        </html>