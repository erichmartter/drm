<html lang="pt-BR" prefix="og: http://ogp.me/ns#" class="no-js"><head>
<meta charset="UTF-8">
<title>Calculadora de despesas do carro · Doutor Multas</title>

<link rel="stylesheet" id="bootstrap-css" href="https://doutormultas.com.br/wp-content/themes/epico-jr/bootstrap/css/bootstrap.min.css?ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="style-css" href="https://doutormultas.com.br/wp-content/themes/epico-jr/style.css?ver=4.9.4" type="text/css" media="all">
<link rel="stylesheet" id="parent-css" href="https://doutormultas.com.br/wp-content/themes/epico/style.min.css?ver=4.9.4" type="text/css" media="all">


<div class="augesystems">

<div class="container">
<div id="title">
<h1>Calculadora de consumo de combustível</h1>
<h2 class="print-only">www.doutormultas.com.br</h2>
<h2 class="screen-only">Preencha os campos abaixo e calcule o consumo do seu veículo.</h2>
</div>



<form method="POST" action="teste2.php">

<div id="selveiculo" class="row">
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
<div class="col-md-12">
<div class="input-group">
<label for="i_carro_valor" class="right" >Valor do veículo</label>
<input type="text" id="valor" for="valor" required="" name="valor" placeholder="Qui">
<h6 style="margin: 0px"  >Não sabe o valor?</h6>
<div class="row screen-only">
<div style="margin-top: -20px" id="consulta_fipe_link" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0">
<a href="https://doutormultas.com.br/fipe/">Consultar tabela FIPE</a>
</div> 
</div>
</div>

<div class="input-group">
<label for="i_carro_valor">Taxa de Juros</label>
<input type="text" id="i_carro_valor" required="" step="1" name="juros"  placeholder="% a.m">
<label for="i_carro_valor">Valor da entrada</label>
<input type="text" id="i_carro_valor" required="" name="entrada" step="1" placeholder="Entrada">
<label for="i_carro_valor">Meses</label>
<input type="text" id="i_carro_valor" required="" name="meses" step="1" placeholder="Nº de vezes">
<button id="processar" type="submit">Calcular</button> <br>

</form>


<?php



if (!empty($_POST["valor"])) {
 $valor = $_POST["valor"];
 $valor = str_replace(".","",$valor);
$valor = str_replace(",",".",$valor);


$juros = $_POST["juros"];
$meses = $_POST["meses"];

$entrada = $_POST["entrada"];
$conta_valor = $valor - $entrada; 
$conta_entrada = $valor - $conta_valor;  
$conta_taxa = ($juros / 100); 

$conta = pow((1 + $conta_taxa), $meses);
$conta = (1 / $conta);
$conta = (1 - $conta);
$conta = ($conta_taxa / $conta);
$parcela = ($conta_valor * $conta);
$total_geral = $conta_entrada + ($parcela * $meses);

$parcela = number_format($parcela, 2, ',', '.');
$valor_entrada = number_format($conta_entrada, 2, ',', '.');
$total_geral = number_format($total_geral, 2, ',', '.');
$conta_valor = number_format($conta_valor, 2, ',', '.');
$juros = ($conta_taxa * 100);
$juros = number_format($juros, 2, ',', '.');

echo "
Entrada de <b>R$ $valor_entrada</b><BR>
 + <b>$meses</b> parcelas de <b>R$ $parcela</b> <BR>
 A uma taxa de <b>$juros%</b> a.m.<BR>
 Valor total será de <b>R$ $total_geral</b>";
}


?>


</div>
</div>
</div>



<div id="dados_veiculo" class="row" style="display: none;">
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
<h2>Resultado</h2>
<div class="row">
<div class="col-md-4">
<div class="col-md-12"><span>Por dia</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_dia">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por mês</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_mes">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por ano</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_ano">R$ 0,00</span></div>
</div>
</div>
<h2>Gastos considerando desvalorizaçao</h2>
<div class="row">
<div class="col-md-4">
<div class="col-md-12"><span>Por dia</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_dia">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por mês</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_mes">R$ 0,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por ano</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_ano">R$ 0,00</span></div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>