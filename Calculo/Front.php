<div class="container">
<div id="title">
<h1>Qual o valor do financiamento do seu veículo?</h1>
<h2 class="print-only">www.doutormultas.com.br</h2>
<h2 class="screen-only">Simule aqui o valor final eo valor das parcelas do seu veículo</h2>
</div>
<div id="selveiculo" class="row">
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
<div class="col-md-12">
<div class="input-group">
<label for="i_carro_valor">Valor do seu veículo</label>
<input type="text" id="i_carro_valor" step="1" placeholder="Valor de venda">
<div class="row screen-only">
<div id="consulta_fipe_link" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0">
<a href="https://doutormultas.com.br/fipe/" target="_blank">Consultar tabela FIPE</a>
</div>
</div>
</div>
</div>
<h2>Quanto você gasta com:</h2>
<div class="row">
<div class="col-md-4">
<div class="input-group">
<label for="i_lavagem">Lavagem</label>
<input type="text" id="i_lavagem" step="1" placeholder="Valor por mês">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_flanelinha">Flanelinha</label>
<input type="text" id="i_flanelinha" step="1" placeholder="Valor por mês">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_estacionamento">Estacionamento</label>
<input type="text" id="i_estacionamento" step="1" placeholder="Valor por mês">
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="input-group">
<label for="i_combustível">Combustível</label>
<input type="text" id="i_combustivel" step="1" placeholder="Valor por mês">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_financiamento">Financiamento</label>
<input type="text" id="i_financiamento" step="1" placeholder="Valor por mês">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_impostos">Taxas e impostos</label>
<input type="text" id="i_impostos" step="1" placeholder="Valor por ano">
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="input-group">
<label for="i_multas">Multas</label>
<input type="text" id="i_multas" step="1" placeholder="Valor por ano">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_manutencao">Manutenção</label>
<input type="text" id="i_manutencao" step="1" placeholder="Valor por ano">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_revisao">Revisões</label>
<input type="text" id="i_revisao" step="1" placeholder="Valor por ano">
</div>
</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="input-group">
<label for="i_seguro">Seguro do veículo</label>
<input type="text" id="i_seguro" step="1" placeholder="Valor por ano">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_franquia">Franquia do seguro</label>
<input type="text" id="i_franquia" step="1" placeholder="Valor por ano">
</div>
</div>
<div class="col-md-4">
<div class="input-group">
<label for="i_outros">Outros</label>
<input type="text" id="i_outros" step="1" placeholder="Valor por ano">
</div>
</div>
</div>
<button id="processar">Calcular</button>
</div>
</div>
<div id="dados_veiculo" class="row" style="">
<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
<h2>Resultado dos gastos</h2>
<div class="row">
<div class="col-md-4">
<div class="col-md-12"><span>Por dia</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_dia">R$0,16</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por mês</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_mes">R$5,00</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por ano</span></div>
<div class="col-md-12"><span class="dados_result" id="sem_desval_ano">R$60,00</span></div>
</div>
</div>
<h2>Gastos considerando desvalorizaçao</h2>
<div class="row">
<div class="col-md-4">
<div class="col-md-12"><span>Por dia</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_dia">R$6,19</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por mês</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_mes">R$188,33</span></div>
</div>
<div class="col-md-4">
<div class="col-md-12"><span>Por ano</span></div>
<div class="col-md-12"><span class="dados_result" id="com_desval_ano">R$2.260,00</span></div>
</div>
</div>
<div class="row">
<button id="btn_imprimir">Imprimir</button>
</div>
</div>
</div>
</div>